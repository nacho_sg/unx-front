import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import VueClipboard from 'vue-clipboard2'

import { routes } from './routes'

Vue.use(VueClipboard);
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
