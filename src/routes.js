import Login from './components/Login.vue';
import Create from './components/Create.vue';
import Copied from './components/Copied.vue';
import Edit from './components/Edit.vue';
import Links from './components/Links.vue';

export const routes = [
  { path: '/login', component: Login },
  { path: '/create/new', component: Edit },
  { path: '/create/:id', component: Create },
  { path: '/create/:id/success', component: Copied },
  { path: '/create/:id/edit', component: Edit },
  { path: '/links', component: Links },
  { path: '*', redirect: '/create/new' },
];