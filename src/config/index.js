export default {
  whatsapp: {
    base: 'https://wa.me/'
  },
  user: {
    email: 'test@test.com',
    password: 'test'
  },
  re: {
    email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  datepicker: {
    es: {
      days: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      pickers: ['siguiente semana', 'siguiente mes', 'semana pasada', 'mes pasado'],
      placeholder: {
        date: 'Seleccione'
      }
    }
  }
}