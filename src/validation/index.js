module.exports = {
  phone(phone) {
    let error;
    if(!phone) {
      error = 'Debe ingresar un celular';
    } else if (phone.length < 10) {
      error = 'Ingrese un número celular de 10 digitos mínimo';
    } else if (phone.match(/[^0-9]/g)) {
      error = 'Solo introduzca números';
    }
    const valid = error ? false : true;
    return { valid, error };
  }
}