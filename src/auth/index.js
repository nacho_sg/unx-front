module.exports = {
  loged (to, from, next) {
    const credentials = JSON.parse(localStorage.getItem('credentials'));
    if (!credentials) {
      return next('/login');
    }
    next();
  },
}